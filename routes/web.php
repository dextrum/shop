<?php

use App\Models\Category;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {

    Route::get('/', function() {
        return view('dashboard');
    });

    Route::resource('section', App\Http\Controllers\CategoryController::class);

    Route::get('/users', function () {
        // Matches The "/admin/users" URL
        return 'hello world';
    })->name('users');
});

require __DIR__.'/auth.php';
