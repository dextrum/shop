<x-app-layout>
    <div class="col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">{{ __('admin.section.create') }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form">
                <div class="card-body">
                    <div class="form-group">
                        <label for="section-title">{{ __('admin.section.title') }}</label>
                        <input type="name" class="form-control" id="section-title" placeholder="{{ __('admin.section.title_placeholder') }}">
                    </div>
                    <div class="form-group">
                        <label for="section-code">{{ __('admin.section.code') }}</label>
                        <input type="name" class="form-control" id="section-code" placeholder="{{ __('admin.section.code_placeholder') }}">
                    </div>
                    <div class="form-group">
                        <label for="section-parent">{{ __('admin.section.parent') }}</label>
                        <select name="" id="section-parent" class="form-control" aria-label="Default select example">
                            <option>{{ __('admin.section.parent_placeholder') }}</option>
                            @foreach ($sections as $section)
                                <option value="{{ $section->id }}">{{ $section->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>