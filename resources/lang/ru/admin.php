<?php

return [

    'homepage' => 'На главную',
    'search' => 'Поиск',
    'products' => 'Продукты',
    'elements' => 'Элементы',
    'sections' => 'Разделы',
    'section' => [
        'create' => 'Создание нового раздела',
        'title' => 'Название раздела',
        'title_placeholder' => 'Введите название раздела',
        'code' => 'Идентификатор',
        'code_placeholder' => 'URL code',
        'parent' => 'Родительский раздел',
        'parent_placeholder' => 'Укажите родительский раздел',
    ],
    'orders' => 'Заказы',
    'users' => 'Пользователи',

];
